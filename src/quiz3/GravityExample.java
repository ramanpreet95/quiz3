package quiz3;

public class GravityExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		gravity on earth
		calculateDistance(9.8,10);
		calculateDistance("Earth",9.8,10);
//		gravity on moon
		calculateDistance(1.62 , 10);
		calculateDistance("MOON",1.62 , 10);
//		gravity on jupiter
		calculateDistance(24.79, 10);
		calculateDistance("jupiter",24.79, 10);
	}
		public static void calculateDistance( double g, int t)
		{
			double distance = 0.5*g*t;
			System.out.println("The distance travelled: "+distance);
			
		
		}
		public static void calculateDistance( String planet,double g, int t)
		{
			double distance = 0.5*g*t;
			System.out.println("The distance travelled on "+planet+":"+distance+"meters");
			
		
		}
	

}
